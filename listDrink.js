class DrinkList {
    constructor(id, maNuocUong, tenNuocUong, donGia, ngayTao, ngayCapNhat) {
        this.id = id;
        this.maNuocUong = maNuocUong;
        this.tenNuocUong = tenNuocUong;
        this.donGia = donGia;
        this.ngayTao = ngayTao;
        this.ngayCapNhat = ngayCapNhat;
    }
}

let drinkClassList = [];

// Khởi tạo các class Drink
let traTacClass = new DrinkList(1, "TRATAC", "Trà tắc", 10000, "14/5/2021", "14/5/2021");
drinkClassList.push(traTacClass);

let cocaClass = new DrinkList(2, "COCA", "Cocacola", 15000, "14/5/2021", "14/5/2021");
drinkClassList.push(cocaClass);

let pepsiClass = new DrinkList(3, "PEPSI", "Pepsi", 15000, "14/5/2021", "14/5/2021");
drinkClassList.push(pepsiClass);

const drinkObjectList = [
    {
        "id": 1,
        "maNuocUong": "TRATAC",
        "tenNuocUong": "Trà tắc",
        "donGia": 10000,
        "ngayTao": "14/5/2021",
        "ngayCapNhat": "14/5/2021"
    },
    {
        "id": 2,
        "maNuocUong": "COCA",
        "tenNuocUong": "Cocacola",
        "donGia": 15000,
        "ngayTao": "14/5/2021",
        "ngayCapNhat": "14/5/2021"
    },
    {
        "id": 3,
        "maNuocUong": "PEPSI",
        "tenNuocUong": "Pepsi",
        "donGia": 15000,
        "ngayTao": "14/5/2021",
        "ngayCapNhat": "14/5/2021"
    }
]

module.exports = {
    drinkClassList,
    drinkObjectList
};