const express = require ('express')

const port = 8000;
const app = express();

//Import DrinkList
const DrinkList = require('./listDrink');

app.get('/', (req, res) => {
    res.json({
        message: 'App Running, add "/drinks-class" or "/drinks-object"'
    })
})

app.get('/drinks-class', (req, res) => {
    res.json({
        drinksClass: DrinkList.drinkClassList
    })
})

app.get('/drinks-object', (req, res) => {
    res.json({
        drinksObject: DrinkList.drinkObjectList
    })
})

app.listen(port, () => {
    console.log('Server started on port ' + port);
})